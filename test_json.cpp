#include "myJson.h"
#include "yyjson.h"
#include <iostream>

int main(void)
{
	
	//------------------生成JSON-------------------//
	MyJson *my_Json = new MyJson;
	my_Json->Init(_WRITE_JSON_);
	my_Json->AddRootKeyStr("cmd", "get_channel_list能加");

	void *arr = my_Json->CreatJsonArrNum(NULL, 0);
	my_Json->AddRootArr(arr, "data");
	
	void *obj = my_Json->CreatJsonObj();
	my_Json->AddObjKeyStr(obj, "taskid", "112201602");
	my_Json->AddObjKeyStr(obj, "taskname","K259+520_HD");
	my_Json->AddObjKeyStr(obj, "address", "192.168.11.41");
	my_Json->AddObjKeyStr(obj, "cameraip", "192.168.2.221");
	my_Json->AddObjKeyNum(obj, "no", 1);
	my_Json->AddObjKeyNum(obj, "run_stat", 1);
	my_Json->AddObjKeyNum(obj, "link_stat", 1);
	my_Json->AddObjKeyNum(obj, "conn_num", 0);
	
	my_Json->AddArrVal(arr, obj);

	char buf[2048] = { 0 };
	my_Json->printJsonStr(buf);
	//printf("%s\n", buf);
   
	my_Json->Free();

	//--------------------解析JSON------------------//
	MyJson *pause_Json = new MyJson;
	pause_Json->Init(_PAUSE_JSON_, buf);

	char ans[1024] = { 0 };
	pause_Json->PauseRootKeyStr("cmd", ans, 1024);
	printf("%s\n", ans);

 	void* arr_obj = pause_Json->PauseRootKeyArr("data");
	void* obj_buf[1];
	pause_Json->PauseArrElemObj(arr_obj, obj_buf, 1);

	/*printf("%s\n", pause_Json->PauseObjElemStr(obj_buf[0], "taskid"));
	printf("%s\n", pause_Json->PauseObjElemStr(obj_buf[0], "taskname"));
	printf("%s\n", pause_Json->PauseObjElemStr(obj_buf[0], "address"));
	printf("%s\n", pause_Json->PauseObjElemStr(obj_buf[0], "cameraip"));*/
	printf("%d\n", pause_Json->PauseObjElemNum(obj_buf[0], "no"));
	printf("%d\n", pause_Json->PauseObjElemNum(obj_buf[0], "run_stat"));
	printf("%d\n", pause_Json->PauseObjElemNum(obj_buf[0], "link_stat"));
	printf("%d\n", pause_Json->PauseObjElemNum(obj_buf[0], "conn_num"));

	pause_Json->Free();
	
	getchar();
	return 0;
}