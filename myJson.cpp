#include "myJson.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// 判断是否是utf8编码
bool MyJson::IsStrUtf8(const char* str)
{
	unsigned int nBytes = 0;//UFT8可用1-6个字节编码,ASCII用一个字节
	unsigned char chr = *str;
	bool bAllAscii = true;
	for (unsigned int i = 0; str[i] != '\0'; ++i) {
		chr = *(str + i);
		//判断是否ASCII编码,如果不是,说明有可能是UTF8,ASCII用7位编码,最高位标记为0,0xxxxxxx
		if (nBytes == 0 && (chr & 0x80) != 0) {

			bAllAscii = false;
		}
		if (nBytes == 0) {
			//如果不是ASCII码,应该是多字节符,计算字节数
			if (chr >= 0x80) {
				if (chr >= 0xFC && chr <= 0xFD) {
					nBytes = 6;
				}
				else if (chr >= 0xF8) {
					nBytes = 5;
				}
				else if (chr >= 0xF0) {
					nBytes = 4;
				}
				else if (chr >= 0xE0) {
					nBytes = 3;
				}
				else if (chr >= 0xC0) {
					nBytes = 2;
				}
				else {
					return false;
				}
				nBytes--;
			}
		}
		else {
			//多字节符的非首字节,应为 10xxxxxx
			if ((chr & 0xC0) != 0x80) {
				return false;
			}
			//减到为零为止
			nBytes--;
		}
	}
	//违返UTF8编码规则
	if (nBytes != 0) {
		return false;
	}
	if (bAllAscii) { //如果全部都是ASCII, 也是UTF8
		return true;
	}
	return true;
}


// 判断是否是gbk编码
bool MyJson::IsStrGbk(const char* str)
{
	unsigned int nBytes = 0;//GBK可用1-2个字节编码,中文两个 ,英文一个
	unsigned char chr = *str;
	bool bAllAscii = true; //如果全部都是ASCII,
	for (unsigned int i = 0; str[i] != '\0'; ++i) {
		chr = *(str + i);
		if ((chr & 0x80) != 0 && nBytes == 0) {// 判断是否ASCII编码,如果不是,说明有可能是GBK
			bAllAscii = false;
		}
		if (nBytes == 0) {
			if (chr >= 0x80) {
				if (chr >= 0x81 && chr <= 0xFE) {
					nBytes = +2;
				}
				else {
					return false;
				}
				nBytes--;
			}
		}
		else {
			if (chr < 0x40 || chr>0xFE) {
				return false;
			}
			nBytes--;
		}//else end
	}
	if (nBytes != 0) {   //违返规则
		return false;
	}
	if (bAllAscii) { //如果全部都是ASCII, 也是GBK
		return true;
	}
	return true;
}

// 注释：多字节包括GBK和UTF-8
int MyJson::GBK2UTF8(char *szGbk, char *szUtf8, int Len)
{

	// 先将多字节GBK（CP_ACP或ANSI）转换成宽字符UTF-16

	// 得到转换后，所需要的内存字符数

	int n = MultiByteToWideChar(CP_ACP, 0, szGbk, -1, NULL, 0);

	// 字符数乘以 sizeof(WCHAR) 得到字节数

	WCHAR *str1 = new WCHAR[sizeof(WCHAR) * n];

	// 转换

	MultiByteToWideChar(CP_ACP,  // MultiByte的代码页Code Page

		0,            //附加标志，与音标有关

		szGbk,        // 输入的GBK字符串

		-1,           // 输入字符串长度，-1表示由函数内部计算

		str1,         // 输出

		n             // 输出所需分配的内存

	);

	n = WideCharToMultiByte(CP_UTF8, 0, str1, -1, NULL, 0, NULL, NULL);

	if (n > Len)

	{

		delete[]str1;

		return -1;

	}

	WideCharToMultiByte(CP_UTF8, 0, str1, -1, szUtf8, n, NULL, NULL);

	delete[]str1;

	str1 = NULL;

	return 0;

}

//UTF-8 GBK
int MyJson::UTF82GBK(char *szUtf8, char *szGbk, int Len)
{

	int n = MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, NULL, 0);

	WCHAR * wszGBK = new WCHAR[sizeof(WCHAR) * n];

	memset(wszGBK, 0, sizeof(WCHAR) * n);

	MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, wszGBK, n);

	n = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);

	if (n > Len)
	{

		delete[]wszGBK;
		return -1;
	}

	WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, szGbk, n, NULL, NULL);

	delete[]wszGBK;

	wszGBK = NULL;

	return 0;

}


MyJson::MyJson()
	:m_mut_doc(NULL), m_mut_root(NULL), m_doc(NULL), m_root(NULL)
{

}

MyJson::~MyJson()
{
	Free();
}

void MyJson::Free()
{
	if (NULL != m_doc)
	{
		yyjson_doc_free(m_doc);
	}

	if (NULL != m_mut_doc)
	{
		yyjson_mut_doc_free(m_mut_doc);
	}

}

bool MyJson::Init(uint8_t flag, char *json_str)
{

	if (_PAUSE_JSON_ == flag || _PAUSE_WRITE_JSON == flag)
	{
		if (NULL == json_str)
		{
			return false;
		}

		// 如果是GBK编码:提前转换,否则中文会崩溃
		if (IsStrGbk(json_str))
		{
			char* utf8_data = new char[strlen(json_str) * 2];
			int utf8_len = GBK2UTF8(json_str, utf8_data, strlen(json_str) * 2) - 1;
			m_doc = yyjson_read(utf8_data, strlen(utf8_data), 0);
			if (NULL == m_doc)
			{
				return false;
			}

			delete[] utf8_data;
		}
		else if(IsStrUtf8(json_str))
		{
			m_doc = yyjson_read(json_str, strlen(json_str), 0);
			if (NULL == m_doc)
			{
				return false;
			}
		}

		m_root = yyjson_doc_get_root(m_doc);
		if (NULL == m_root)
		{
			return false;
		}
		
	}


	if (_WRITE_JSON_ == flag || _PAUSE_WRITE_JSON == flag)
	{
		m_mut_doc = yyjson_mut_doc_new(NULL);
		if (NULL == m_mut_doc)
		{
			return false;
		}

		m_mut_root = yyjson_mut_obj(m_mut_doc);
		if (NULL == m_mut_root)
		{
			return false;
		}

		yyjson_mut_doc_set_root(m_mut_doc, m_mut_root);

	}


	return true;
}

bool MyJson::AddRootKeyStr(const char *key, const char* value)
{

	return yyjson_mut_obj_add_str(m_mut_doc, m_mut_root, key, value);

}

bool MyJson::AddRootKeyNum(const char *key, int value)
{

	return yyjson_mut_obj_add_int(m_mut_doc, m_mut_root, key, value);

}

bool MyJson::AddRootobj(void* obj, const char *key)
{
	return yyjson_mut_obj_add_val(m_mut_doc, m_mut_root, key, (yyjson_mut_val*)obj);
}

bool MyJson::AddRootArr(void *arr, const char *key)
{

	return yyjson_mut_obj_add_val(m_mut_doc, m_mut_root, key, (yyjson_mut_val*)arr);

}

void* MyJson::CreatJsonArrNum(const int32_t *buf, int elemCount)
{
	yyjson_mut_val* arr = yyjson_mut_arr_with_sint32(m_mut_doc, buf, elemCount);
	return (void*)arr;
}


void* MyJson::CreatJsonObj()
{
	yyjson_mut_val* obj = yyjson_mut_obj(m_mut_doc);
	return (void*)obj;
}


bool MyJson::AddArrVal(void *arr, void* val)
{

	return yyjson_mut_arr_add_val((yyjson_mut_val *)arr, (yyjson_mut_val *)val);

}

bool MyJson::printJsonStr(char *json_str)
{

	const char *json = yyjson_mut_write(m_mut_doc, 0, NULL);
	if (json)
	{
		strcpy_s(json_str, _MAX_PATH, json);
		free((void *)json);
		return true;
	}

	return false;
}

bool MyJson::AddObjKeyStr(void *val, const char* key, const char* value)
{

	return yyjson_mut_obj_add_str(m_mut_doc, (yyjson_mut_val *)val, key, value);

}

bool MyJson::AddObjKeyNum(void *val, const char* key, int value)
{
	return yyjson_mut_obj_add_int(m_mut_doc, (yyjson_mut_val *)val, key, value);
}


//------------------------------------------------------解析-------------------------------------------//
bool MyJson::PauseRootKeyStr(const char *key, char *val, int vallen)
{
	yyjson_val *key_val = yyjson_obj_get(m_root, key);

	const char *src = yyjson_get_str(key_val);

	char* tmp = (char *)malloc(sizeof(char) * strlen(src) + 1);
	strcpy_s(tmp, strlen(src) + 1, src);

	UTF82GBK(tmp, val, vallen);

	free(tmp);

	return true;
}

int MyJson::PauseRootKeyNum(const char *key)
{

	yyjson_val *key_val = yyjson_obj_get(m_root, key);
	return yyjson_get_int(key_val);
}

void* MyJson::PauseRootKeyArr(const char *key)
{
	yyjson_val *hits = yyjson_obj_get(m_root, key);
	return (void*)hits;
}

bool MyJson::PauseArrElemStr(void *arr, char* buf[], int buflen, int bufCount)
{
	size_t idx, max;
	yyjson_val *hit;
	size_t len = yyjson_arr_size((yyjson_val *)arr);
	for (size_t idx = 0; idx < len; ++idx)
	{
		if (idx >= bufCount)
		{
			break;
		}

		hit = yyjson_arr_get((yyjson_val *)arr, idx);

		strcpy_s(buf[idx], _MAX_PATH, yyjson_get_str(hit));

		const char *src = yyjson_get_str(hit);
		char* tmp = (char *)malloc(sizeof(char) * strlen(src) + 1);
		strcpy_s(tmp, strlen(src) + 1, src);

		UTF82GBK(tmp, buf[idx], buflen);

		free(tmp);
	}

	return true;
}

bool MyJson::PauseArrElemObj(void *arr, void *buf[], int bufCount)
{
	size_t idx, max;
	yyjson_val *hit;
	size_t len = yyjson_arr_size((yyjson_val *)arr);
	for (size_t idx = 0; idx < len; ++idx)
	{
		if (idx >= bufCount)
		{
			break;
		}

		buf[idx] = yyjson_arr_get((yyjson_val *)arr, idx);
	}

	return true;
}

bool MyJson::PauseArrElemNum(void *arr, int *buf, int bufCount)
{
	size_t idx, max;
	yyjson_val *hit;
	size_t len = yyjson_arr_size((yyjson_val *)arr);
	for (size_t idx = 0; idx < len; ++idx)
	{
		if (idx >= bufCount)
		{
			break;
		}

		hit = yyjson_arr_get((yyjson_val *)arr, idx);
		buf[idx] = yyjson_get_int(hit);
	}

	return true;
}

void* MyJson::PauseRootKeyObj(const char *key)
{
	yyjson_val *obj = yyjson_obj_get(m_root, key);
	return (void*)obj;
}

bool MyJson::PauseObjElemStr(void *obj, const char *key, char *val, int vallen)
{
	yyjson_val *key_val = yyjson_obj_get((yyjson_val *)obj, key);

	const char *src = yyjson_get_str(key_val);

	char* tmp = (char *)malloc(sizeof(char) * strlen(src) + 1);
	strcpy_s(tmp, strlen(src) + 1, src);

	UTF82GBK(tmp, val, vallen);

	free(tmp);

	return true;
}

int MyJson::PauseObjElemNum(void *obj, const char *key)
{
	yyjson_val *key_val = yyjson_obj_get((yyjson_val *)obj, key);
	return yyjson_get_int(key_val);
}