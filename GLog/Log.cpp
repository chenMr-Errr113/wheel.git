#include "Log.h"

// 静态变量初始化
LOG*         LOG::Log         = NULL;
char*        LOG::logFilePath = NULL;
static std::mutex  log_mutex;

LOG::LOG()
{
   
}


LOG::~LOG()
{

}

void LOG::Init(_log_target_ logTarget, char* fileName)
{

    if(NULL == Log)
    {
        log_mutex.lock();
        if(NULL == Log)
        {
            Log = new LOG();
        }
        log_mutex.unlock();
    }

    setLogTarget(logTarget);
    
    // 如果日志打印到文件,则输出到文件
    if(LOG_TARGET_FILE == logTarget)
    {
        createFile(fileName);
    }
    
}

// 请不要轻易调用
void LOG::unInit(int type)
{
    if(NULL != logFilePath)
    {
        delete logFilePath;
        logFilePath = NULL;
    }

    if(type == _DESTROY_)
    {
        if(NULL == Log)
        {
            delete Log;
            Log = NULL;
        }
    }
}


int LOG::createFile(char* fileName)
{
    
    char* logDir = getcwd(NULL, 0);
    char logDirPath[125];
    strncpy(logDirPath, logDir, strlen(logDir) - 4); 
    strcat(logDirPath, "Log");

    // 如果文件夹不存在,则创建一个文件夹
    if(0 != access(logDirPath, F_OK))
    {
        printf("LOG::createDir[INFO]::%s\n", "日志文件存放目录不存在");
        if(0 != mkdir(logDir, 0777))
        {
            printf("LOG::createDir[ERROR]::%s\n", "日志文件目录创建失败");
        }
        else
        {
            printf("LOG::createDir[INFO]::%s\n", "日志文件目录创建成功");
        }
    }    
    
    // 创建文件
    // 获取当前时间
    timeval tv;
    gettimeofday(&tv, NULL);
    time_t sec = tv.tv_sec;
    tm* lt = localtime(&sec);  // 当地时间
    char tiemPath[100] = {0};
    snprintf(tiemPath, 256, "%d-%02d-%02d %02d:%02d:%02d", lt->tm_year + 1900, lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);

    char logFile[256] = {0};
    snprintf(logFile, 256, "%s/%s-%s%s", logDirPath, (char*)fileName, tiemPath, ".log");
    
    FILE* fd = NULL;
    if(0 != access(logFile, F_OK))
    {
        if(fd == fopen(logFile, "w+"))
        {
            printf("LOG::createDir[ERROR]::%s\n", "创建文件失败");
        }
    }
    else
    {
        // 如果存在则删除文件
        remove(logFile);
        if(fd == fopen(logFile, "w+"))
        {
            printf("LOG::createDir[ERROR]::%s\n", "创建文件失败");
        }
    }
    
    log_mutex.lock();
    logFilePath = (char*)malloc(sizeof(logFile));
    if(NULL == logFilePath)
    {
        printf("LOG::createDir[ERROR]::%s\n", "分配内存失败");
    }

    strcpy(logFilePath, logFile);
    log_mutex.unlock();

    if(NULL != fd)
    {
        fclose(fd);
    }
    

    return 0;
}


LOG* LOG::getInstance()
{
    if(NULL == Log)
    {
        log_mutex.lock();
        if(NULL == Log)
        {
            Log = new LOG();
        }
        log_mutex.unlock();
    }

    return Log;
}

_log_level_ LOG::getLogLevel()
{
    return this->logLevel;
}

void LOG::setLogLevel(_log_level_ ilogLevel)
{
    this->logLevel = ilogLevel;
}

_log_target_ LOG::getLogTarget()
{
    return this->logTarget;
}

void LOG::setLogTarget(_log_target_ ilogTarget)
{
    this->logTarget = ilogTarget;
}


int LOG::writeLog(_log_level_ logLevel, char* fileName, int number, char* format, ...)
{
    int ret = 0;

    // 获取日期和时间
    // 获取当前时间
    timeval tv;
    gettimeofday(&tv, NULL);
    time_t sec = tv.tv_sec;
    tm* lt = localtime(&sec);  // 当地时间
    char tiemPath[100] = {0};
    snprintf(tiemPath, 256, "%d-%02d-%02d %02d:%02d:%02d", lt->tm_year + 1900, lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);
    
    // LOG级别
    char* plogLevel = NULL;
    char buf[2048] = {0};
    if(logLevel == LOG_LEVEL_DEBUG)
    {
        plogLevel = "DEBUG";
        sprintf(buf, "[%s][pid:%02d][tid:%d]::%s %04d\n", "DEBUG", getpid(), pthread_self(), fileName, number);
    }
    else if(logLevel == LOG_LEVEL_INFO)
    {
        plogLevel = "INFO";
        sprintf(buf, "[%s][pid:%02d][tid:%d]::%s %04d\n", "INFO", getpid(), pthread_self(), fileName, number);
    }
    else if(logLevel == LOG_LEVEL_WARNING)
    {
        plogLevel = "WARNING";
        sprintf(buf, "[%s][pid:%02d][tid:%d]::%s %04d\n", "WARNING", getpid(), pthread_self(), fileName, number);
    }
    else if(logLevel == LOG_LEVEL_ERROR)
    {
        //  [进程号][线程号][Log级别][文件名][函数名:行号]
        sprintf(buf, "[%s][pid:%02d][tid:%d]::%s %04d\n", "ERROR", getpid(), pthread_self(), fileName, number);
    }
    
    // 获取描述信息
    char logInfo[1024] = {0};
    va_list ap;
    va_start(ap, format);
    ret = vsnprintf(logInfo, 1024, format, ap);
    va_end(ap);
    
    strncat(buf, logInfo, strlen(logInfo));

    // 写文件
    int fd = open(logFilePath, O_RDWR | O_APPEND);
    if(-1 == fd)
    {
        printf("LOG::createDir[ERROR]::%s\n", "文件打开失败");
    }
    
    if('\n' != buf[strlen(buf) - 1])
    {
        strcat(buf, "\n-----------------------------------------------\n");
    }
    
    log_mutex.lock();
    if(LOG::getInstance()->getLogTarget() & LOG_TARGET_FILE)
    {
        write(fd, buf, strlen(buf));
        close(fd);
    }

    if(LOG::getInstance()->getLogTarget() & LOG_TARGET_CONSOLE)
    {
        printf("%s", buf);
    }
    log_mutex.unlock();


    return 0;

}


