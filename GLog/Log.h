/*-----------------------------------------------------------
创建时间: 2023-2-11
作者: 陈鑫
用途: 个人日志系统 
说明: 日志信息包含关键变量的值、运行的位置(哪个文件、哪个函数、哪一行)
      时间-线程号-进程号
日志级别:
    开发阶段:开发人员需要尽可能详细的追踪代码的运行过程,所以可以打印尽可能多的信息到日志文件中
    测试节点:可能需要过滤掉一些信息,这时候有的信息可能不必要输出到Log文件中,产品交付客户使用时,
            为了软件运行更快、客户体验更好,这时候就只需要打印关键信息到日志文件。

扩展:
    在实际的开发过程中,通常可以在配置文件中设置Log级别,方便在开发、调试、测试和客户现场灵活地调整日志级别,获取有用的日志信息

输出的位置:
    Log文件可以输出到控制台或者文件中(根据具体的需求,可以设置成可配置的方式)。
    通常情况下,我们会选择输出到文件:更多信息、更快

-------------------------------------------------------------*/


#ifndef _LOG_H_
#define _LOG_H_

#include <stdarg.h>
#include <stdio.h>
#include <mutex>
#include <string>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

// #include <handleapi.h>
// #include <windows.h>
// #include <tchar.h>
// #include <direct.h>

#define _DESTROY_     1
#define _UN_DESTROY_  0


// 定义日志级别
typedef enum
{

    LOG_LEVEL_NONE,                // 无
    LOG_LEVEL_ERROR,               // 错误信息
    LOG_LEVEL_WARNING,             // 警告信息
    LOG_LEVEL_DEBUG,               // 调试信息
    LOG_LEVEL_INFO                 // 提示信息

} _log_level_;


// 日志输出方式
typedef enum
{
    LOG_TARGET_NONE    = 0x00,      // 日志不输出   
    LOG_TARGET_CONSOLE = 0x01,      // 日志输出到控制台
    LOG_TARGET_FILE    = 0x10       // 日志输出到文件

} _log_target_;


// 日志类:采用单例模式(构造函数私有化),并且全局
class LOG
{
public:
    
    // 初始化
    void Init(_log_target_ logTarget, char* fileName);

    // 释放
    void unInit(int type = _UN_DESTROY_);

    // 文件
    int createFile(char* fileName);

    // 获取Log
    static LOG* getInstance();

    // Log打印
    int writeLog(_log_level_ logLevel, char* fileName, int number, char* format, ...);

    // Log级别
    _log_level_ getLogLevel();
    void setLogLevel(_log_level_ logLevel);
   
    // Log输出位置
    _log_target_ getLogTarget();
    void setLogTarget(_log_target_ logTarget);

private:
    
    LOG();
    ~LOG();

    static LOG*        Log;
 
    static std::string logBuffer;
    static char*       logFilePath;

    _log_level_        logLevel;
    _log_target_       logTarget;

};


#endif