#include "WaHashMap.h"
#include <stdlib.h>
//#include <Windows.h>
#include <stdio.h>
#include "WindowsApi.h"

//hash值的范围
#define	HASH_NODE_KEY_SIZE (1024*1024)  
//最大结点个数（避免内存分配失败,可根据情况设置）
#define	HASH_NODE_MAX_NUM  (1024*1024) 
//最小结点个数
#define	HASH_NODE_MIN_NUM  (4)	       


//-----------------------------------------------------
//说明: 初始化hashmap管理器,分配内存资源等
//输入: uMaxNodeCount 最大分配hash节点个数(注意计算空间,内部需要分配内存空间)
//输入: uDataNodeSize 数据节点空间大小
//输入: HashNodeCmp  节点比较函数指针
//输入: HashNodeKey  计算hash函数指针
//输入: HashNodeCopy 节点拷贝函数指针
//输入: HashTimeOut 处理超时节点数据的函数指针
//输入: pOutMsg 输出信息(失败时标识错误原因)
//输入: uMaxOutSize 最大输出空间大小
//返回: hashmap管理器指针
//-----------------------------------------------------
WaHashMap *InitHashMap(uint32 uMaxNodeCount, uint32 uDataNodeSize,
					   Fun_HashNodeCmp HashNodeCmp, Fun_HashNodeKey HashNodeKey,
					   Fun_HashNodeCopy HashNodeCopy, Fun_HashTimeOut HashTimeOut,
					   uint16 *pOutMsg, uint32 uMaxOutSize)
{
	uint8 *pData = NULL;
	uint8 *pTmpMem = NULL;
	uint32 uMemCount = 0;
	uint32 uIndex = 0;
	WaHashMap *pHashMap = NULL;

	if ((NULL == HashNodeCmp) || (NULL == HashNodeKey) || (NULL == HashNodeCopy) || 
		(NULL == HashTimeOut) || (NULL == pOutMsg))
	{
		return NULL;
	}

	if ((uMaxNodeCount > HASH_NODE_MAX_NUM) || (uMaxNodeCount < HASH_NODE_MIN_NUM))
	{
		swprintf_s(pOutMsg,uMaxOutSize-1,L"InitHashMap失败,节点个数%u错误,必须在%u和%u之间",
			uMaxNodeCount,HASH_NODE_MIN_NUM,HASH_NODE_MAX_NUM);
		return NULL;
	}

	//计算总空间大小
	uMemCount = sizeof(WaHashMap) + HASH_NODE_KEY_SIZE * sizeof(WaHashNode*)  +
		uMaxNodeCount * sizeof(WaHashNode) ;

	//分配内存空间
	pHashMap = (WaHashMap*)malloc(uMemCount);
	if (NULL == pHashMap)
	{
		swprintf_s(pOutMsg,uMaxOutSize-1,L"InitHashMap失败,分配hash管理内存空间%uM失败,err = %u", 
			uMemCount/1024/1024, GetLastError());
		return NULL;
	}

	//数据清理,这里有必要,否则其中的标志位可能是错误的
	pTmpMem = (uint8*)pHashMap;
	memset(pTmpMem,0, uMemCount);

	//分配数据节点空间
	pHashMap->pDataBuff = (uint8*)malloc(uDataNodeSize * uMaxNodeCount);
	if (NULL == pHashMap->pDataBuff)
	{
		swprintf_s(pOutMsg,uMaxOutSize-1,L"InitHashMap失败,分配数据节点内存空间%uM失败,err = %u", 
			(uDataNodeSize * uMaxNodeCount)/1024/1024, GetLastError());
		free(pHashMap);
		pHashMap = NULL;
		return NULL;
	}
	memset(pHashMap->pDataBuff,0, uDataNodeSize * uMaxNodeCount);

	pTmpMem += sizeof(WaHashMap);
	pHashMap->pHash =(WaHashNode**)pTmpMem;

	pTmpMem += HASH_NODE_KEY_SIZE * sizeof(WaHashNode *);
	pHashMap->pHead = (WaHashNode*)pTmpMem;

	pHashMap->pCurHead = pHashMap->pHead;
	pTmpMem += uMaxNodeCount * sizeof(WaHashNode);

	pHashMap->uDataNodeSize = uDataNodeSize;

	//循环将链表连接起来
	for (uIndex = 1 ; uIndex < uMaxNodeCount - 1; uIndex++)
	{
		//重置使用标识
		pHashMap->pHead[uIndex].u16Flag = 0;
		pHashMap->pHead[uIndex].pDataNode = (uint8*)(pHashMap->pDataBuff + uIndex*uDataNodeSize); 
		pHashMap->pHead[uIndex].next = &(pHashMap->pHead[uIndex + 1]);
		pHashMap->pHead[uIndex].prev = &(pHashMap->pHead[uIndex - 1]);
	}
	pHashMap->pHead[0].u16Flag = 0;
	pHashMap->pHead[0].pDataNode = pHashMap->pDataBuff;
	pHashMap->pHead[0].next = &(pHashMap->pHead[1]);
	pHashMap->pHead[0].prev = &(pHashMap->pHead[uMaxNodeCount - 1]);

	pHashMap->pHead[uMaxNodeCount - 1].u16Flag = 0;
	pHashMap->pHead[uMaxNodeCount - 1].pDataNode = (pHashMap->pDataBuff + (uMaxNodeCount - 1)*uDataNodeSize); 
	pHashMap->pHead[uMaxNodeCount - 1].next = &(pHashMap->pHead[0]);
	pHashMap->pHead[uMaxNodeCount - 1].prev = &(pHashMap->pHead[uMaxNodeCount - 2]);

	//外部提供计算函数指针
	pHashMap->HashNodeCmp = HashNodeCmp;
	pHashMap->HashNodeKey = HashNodeKey;
	pHashMap->HashNodeCopy = HashNodeCopy;
	pHashMap->HashTimeOut = HashTimeOut;

	//重置统计计数
	pHashMap->uMaxNodeCount = uMaxNodeCount;
	pHashMap->uCurUseCount = 0;
	pHashMap->uDelNodeCount = 0;
	pHashMap->uOverNodeCount = 0;

	//重置冲突率统计
	memset(pHashMap->uStatFindCount,0,sizeof(pHashMap->uStatFindCount));

	return pHashMap;
}

//-----------------------------------------------------
//说明: 释放hashmap管理器资源（释放内部内存资源）
//输入: hashmap管理器指针
//返回: 无
//-----------------------------------------------------
void FreeHashMap(WaHashMap **pHashMap)
{
	if ((NULL != pHashMap) && (NULL != *pHashMap))
	{
		if (NULL != (*pHashMap)->pDataBuff)
		{
			free((*pHashMap)->pDataBuff);
			(*pHashMap)->pDataBuff = NULL;
		}
		free(*pHashMap);
		*pHashMap = NULL;
	}

	return;
}

//-----------------------------------------------------
//说明: 新建一个结点
//输入: pHashMap 管理器指针
//输入: pNode 需要插入的节点数据
//输入: u64LastTime 插入节点的时间(外部传入比较好,跑离线数据时用系统时间是错误的)
//返回: 插入的节点的指针,失败返回NULL
//-----------------------------------------------------
WaHashNode *HashBuildNode(WaHashMap *pHashMap, void *pDataNode, uint64 u64LastTime)
{
	WaHashNode* pTmpNode = NULL;

	if ((NULL == pHashMap) || (NULL == pDataNode))
	{
		return NULL;
	}

	//判断是否在hash链表中使用，如果在使用要删除。只有一个结点的链路，没有前后指针
	if(pHashMap->pCurHead->u16Flag)
	{
		//如果有,说明这个结点是超时的结点,处理数据
		pHashMap->HashTimeOut(pHashMap->pCurHead->pDataNode);
		pHashMap->uOverNodeCount++;
		
		//删除原来的结点
		HashRemoveNode(pHashMap, pHashMap->pCurHead);
	}

	//计数统计
	pHashMap->uCurUseCount++;

	//拷贝数据区
	pHashMap->HashNodeCopy(pHashMap->pCurHead->pDataNode,pDataNode);

	//设置时间
	pHashMap->pCurHead->u64LastTime = u64LastTime;

	//插入节点,这里不判断失败,除了参数错,其他不会失败
	HashInsetNode(pHashMap, pHashMap->pCurHead);
	
	pHashMap->pCurHead = pHashMap->pCurHead->next; 
	return pHashMap->pCurHead->prev;
}

//-----------------------------------------------------
//说明: 插入一个节点
//输入: pHashMap 管理器指针
//输入: pNode 需要插入的节点
//返回: 插入的节点的指针,失败返回NULL
//-----------------------------------------------------
WaHashNode *HashInsetNode(WaHashMap *pHashMap, WaHashNode *pNode)
{
	uint32 uKey = 0;
	WaHashNode *pTmpNode = NULL;

	if ((NULL == pHashMap) || (NULL == pNode))
	{
		return NULL;
	}

	//计算hash值
	uKey = pHashMap->HashNodeKey(pNode->pDataNode) & (HASH_NODE_KEY_SIZE - 1);	

	//得到对应节点指针
	pTmpNode = pHashMap->pHash[uKey] ;

	//将最新的一个插入对应hash值的节点链表的头部
	pNode->list_prev = NULL;
	pNode->list_next = pTmpNode;
	if (NULL != pTmpNode)
	{
		pTmpNode->list_prev = pNode;
	}
	pHashMap->pHash[uKey] = pNode;

	//重置使用标识
	pNode->u16Flag = 1;

	//最老使用节点
	if (pHashMap->pCurLast == pHashMap->pCurHead)
	{
		pHashMap->pCurLast = pHashMap->pCurLast->next;
	}
	else if (NULL == pHashMap->pCurLast)
	{
		pHashMap->pCurLast = pHashMap->pCurHead;
	}

	//测试代码,容错处理
	if (0 == pHashMap->pCurLast->u16Flag)
	{
		pHashMap->pCurLast = NULL;
	}

	return pNode;
}


//-----------------------------------------------------
//说明: 将结点移除：从链表中移除,不是hash表中
//输入: pHashMap 管理器指针
//输入: pNode 需要移除的节点
//返回: 无
//-----------------------------------------------------
int32 HashRemoveNode(WaHashMap *pHashMap, WaHashNode *pNode)
{
	uint32 uKey = 0;

	if ((NULL == pHashMap) || (NULL == pNode))
	{
		return -1;
	}

	//计算hash值
	uKey = pHashMap->HashNodeKey(pNode->pDataNode) & (HASH_NODE_KEY_SIZE - 1);

    //没有在链表中
	if (NULL == pHashMap->pHash[uKey])		
	{
		pNode->list_prev = NULL;
		pNode->list_next = NULL;
		return -1;
	}

	//这步处理避免环状链表循环覆盖,解决根源问题
	if (pHashMap->pCurLast== pHashMap->pCurHead)
	{
		//只有一个节点
		if (1 == pHashMap->uCurUseCount)
		{
			pHashMap->pCurLast = NULL;
		}
		//节点满,将要被覆盖时
		else if (pHashMap->uMaxNodeCount == pHashMap->uCurUseCount)
		{
			pHashMap->pCurLast = pHashMap->pCurLast->next;
		}
	}

	//最老使用节点
	if (pHashMap->pCurLast == pNode)
	{
		if (pHashMap->pCurLast->next != pHashMap->pCurHead)
		{
			pHashMap->pCurLast = pHashMap->pCurLast->next;
		}
		else
		{
			pHashMap->pCurLast = NULL;
		}
	}
	
	//统计计数
	pHashMap->uCurUseCount--;
	pHashMap->uDelNodeCount++;

	//重置使用标识
	pNode->u16Flag = 0;

	//
	if (pNode != pHashMap->pCurHead)
	{
		pNode->next->prev = pNode->prev;
		pNode->prev->next = pNode->next;
		pNode->prev = pHashMap->pCurHead;
		pNode->next = pHashMap->pCurHead->next;
		pHashMap->pCurHead->next->prev = pNode;
		pHashMap->pCurHead->next = pNode;
	}

	//表示是链表头指针
	if (NULL == pNode->list_prev)
	{
		// 删除链表头
		pHashMap->pHash[uKey] = pNode->list_next;
		if (NULL != pHashMap->pHash[uKey])
		{
			pHashMap->pHash[uKey]->list_prev = NULL; 
		}
		pNode->list_prev = NULL;
		pNode->list_next = NULL;
		return 0;
	}

	//在链表末尾
	if (NULL == pNode->list_next)
	{
		pNode->list_prev->list_next = NULL;
		pNode->list_prev = NULL;
		pNode->list_next = NULL;
		return 0;
	}

	//在链表中间
	pNode->list_prev->list_next = pNode->list_next;
	pNode->list_next->list_prev = pNode->list_prev;
	pNode->list_prev = NULL;
	pNode->list_next = NULL;


	return 0;
}


//-----------------------------------------------------
//说明: 查找一个hash节点
//输入: pHashMap 管理器指针
//输入: pNode 需要查找的节点
//返回: 成功返回查找到的节点指针,失败返回NULL
//-----------------------------------------------------
WaHashNode *HashFindNode(WaHashMap *pHashMap, void *pDataNode)
{
	uint32 uKey = 0;
	uint32 uStatCount = 0;
	WaHashNode *pTmpNode = NULL;

	if ((NULL == pHashMap) || (NULL == pDataNode))
	{
		return NULL;
	}
	uKey = pHashMap->HashNodeKey(pDataNode) & (HASH_NODE_KEY_SIZE - 1);
	pTmpNode = pHashMap->pHash[uKey];
	while (NULL != pTmpNode)
	{
		uStatCount++;

		//注意,这里的比较函数是外部指定的,要求必须相等返回0 ,不相等返回 -1
		if (0 == pHashMap->HashNodeCmp(pTmpNode->pDataNode, pDataNode))
		{
			//最老使用节点
			if (pTmpNode == pHashMap->pCurLast)
			{
				if (pHashMap->pCurLast->next != pHashMap->pCurHead)
				{
					pHashMap->pCurLast = pHashMap->pCurLast->next;
					//测试代码,容错处理
					if (0 == pHashMap->pCurLast->u16Flag)
					{
						pHashMap->pCurLast = NULL;
					}
				}
			}

			// pTmpNode插入CurHead之前,把最新的放在最前面,那么末尾就是最老的,便于查找超时的
			if (pTmpNode == pHashMap->pCurHead)
			{
				pHashMap->pCurHead = pHashMap->pCurHead->next;
				break;
			}
			pTmpNode->next->prev = pTmpNode->prev;
			pTmpNode->prev->next = pTmpNode->next;
			pTmpNode->next = pHashMap->pCurHead;
			pTmpNode->prev = pHashMap->pCurHead->prev;
			pHashMap->pCurHead->prev->next = pTmpNode;
			pHashMap->pCurHead->prev = pTmpNode;
			break;
		}
		pTmpNode = pTmpNode->list_next;
	}

	//统计冲突率,要几次查找才能找到节点,实际中根据情况是否开启
	if (uStatCount > 9)
	{
		pHashMap->uStatFindCount[9]++;
	}
	else
	{
		pHashMap->uStatFindCount[uStatCount] ++;
	}

	return pTmpNode;
}


//-----------------------------------------------------
//说明: 删除超时结点，调用指定的回调函数处理超时出来的数据
//输入: pHashMap 管理器指针
//输入: u64OutTime 设定多少秒算超时,设置为0就超时出所有
//输入: u64CurTime 当前时间
//返回: 返回超时的节点个数,失败返回-1
//-----------------------------------------------------
int32 HashDeleteTimeOut(WaHashMap *pHashMap, uint64 u64OutTime, uint64 u64CurTime)
{
	int32 iRetVal = -1;
	int32 iTimeOutCount = 0;
	WaHashNode *pLast = NULL;

	if (NULL == pHashMap)
	{
		return -1;
	}

	//超时,从last指针反向查找,节约时间,避免循环遍历链表
	pLast = pHashMap->pCurLast;
	while(NULL != pLast)
	{
		//last指针保证顺序以及在使用,这里不再判断uFlag标识

		//查看是否超时
		if ((0 == u64OutTime) || ((u64CurTime - pLast->u64LastTime) > u64OutTime))	
		{
			//处理节点数据
			pHashMap->HashTimeOut(pLast->pDataNode);

			//删除结点
			iRetVal = HashRemoveNode(pHashMap, pLast);
			if (0 != iRetVal)
			{
				//异常情况了,last指向了错误的节点,导致这里死循环,临时处理为NULL
				pHashMap->pCurLast = NULL;
				break;
			}
			else
			{
				iTimeOutCount++;
			}
		}
		else
		{
			break;
		}	

		//删除节点时移动了指针,这里重新赋值
		pLast = pHashMap->pCurLast;
	}
	
	return iTimeOutCount;
}


//-----------------------------------------------------
//说明: 输出计数统计和查找冲突率计数
//输入: pHashMap 管理器指针
//输出: pStatMsg 输出格式化后的状态信息地址
//输入: uMaxOutSize 最大输出空间大小
//返回: 无
//-----------------------------------------------------
void GetHashRunStat(WaHashMap *pHashMap, uint16 *pStatMsg, uint32 uMaxOutSize)
{
	swprintf_s(pStatMsg,uMaxOutSize-1,
		L"HashMap总节点数%u ,使用%u ,已删除%u ,被挤出%u ,查找冲突率 %u %u %u %u %u %u %u %u %u %u",
		pHashMap->uMaxNodeCount,pHashMap->uCurUseCount,
		pHashMap->uDelNodeCount,pHashMap->uOverNodeCount,
		pHashMap->uStatFindCount[0],pHashMap->uStatFindCount[1],pHashMap->uStatFindCount[2],
		pHashMap->uStatFindCount[3],pHashMap->uStatFindCount[4],pHashMap->uStatFindCount[5],
		pHashMap->uStatFindCount[6],pHashMap->uStatFindCount[7],pHashMap->uStatFindCount[8],
		pHashMap->uStatFindCount[9]);

	return;
}