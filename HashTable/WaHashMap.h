

#ifndef _GLOBAL_HASH_MAP_FRAME_WORK_H__
#define _GLOBAL_HASH_MAP_FRAME_WORK_H__

//--------------------------------------------------------
// 本模块主要功能为提供hashmap模板框架,多地方都可使用,需要自己
// 提供hash计算函数,比较函数,拷贝函数等
//--------------------------------------------------------
// 注意:外部提供的Fun_HashNodeCmp系列函数都是成功返回 0 失败返回 -1
//      内部按照这个逻辑进行处理的,如果不匹配则出现问题
//----------------------------------------------------------

#include "WaGlobal.h"


//-----------------------------------------------------
//说明: 数据节点比较函数(外部提供）
//输入: pDstNode,pSrcNode 需要比较的两个数据节点指针
//返回: 相等 0 不等 -1
//-----------------------------------------------------
typedef int32  (*Fun_HashNodeCmp)(void *pDstNode, void *pSrcNode);

//-----------------------------------------------------
//说明: 数据节点计算hash值函数(外部提供）
//输入: pDataNode 数据节点指针
//返回: 计算出的hash的key值
//-----------------------------------------------------
typedef uint32 (*Fun_HashNodeKey)(void *pDataNode);

//-----------------------------------------------------
//说明: 数据节点拷贝函数(外部提供）
//输入: pDstNode 拷贝到的目的节点指针
//输入: pSrcNode 被拷贝的源数据节点指针
//返回: 成功 0 失败 -1
//-----------------------------------------------------
typedef int32  (*Fun_HashNodeCopy)(void *pDstNode, void* pSrcNode);

//-----------------------------------------------------
//说明: 数据节点超时处理函数(外部提供）
//输入: pDataNode 数据节点指针
//返回: 成功 0 失败 -1
//-----------------------------------------------------
typedef int32  (*Fun_HashTimeOut)(void *pDataNode);



#pragma pack(push,1)

//hash节点
typedef	struct _WaHashNode
{
	uint64 u64LastTime;        //最后处理时间
	uint16 u16Flag;            //使用标志位
	uint8 *pDataNode;          //数据节点指针
	struct _WaHashNode *next;  //hash环状双向链表中的指针
	struct _WaHashNode *prev;
	struct _WaHashNode *list_next; //横向链表的指针:指向一条链路的下一个数据
	struct _WaHashNode *list_prev;
}WaHashNode;

//hashmap管理结构
typedef struct _WaHashMap
{
	WaHashNode **pHash;	      //hash表
	WaHashNode *pHead;	      //结点buf
	WaHashNode *pCurHead;	  //最长时间没有被操作的一个
	WaHashNode *pCurLast;     //最长时间未使用的
	uint8      *pDataBuff;    //数据区内存指针
	uint32 uDataNodeSize;     //数据节点空间大小
	uint32 uMaxNodeCount;	  //分配结点个数
	uint32 uCurUseCount;	  //当前使用个数
	uint32 uDelNodeCount;     //删除结点个数
	uint32 uOverNodeCount;    //被挤出结点个数
	uint32 uStatFindCount[10];//统计hash查找时冲突率(实际运行时注释掉)
	Fun_HashNodeCmp  HashNodeCmp; //节点比较函数指针
	Fun_HashNodeKey  HashNodeKey; //计算hash函数指针
	Fun_HashNodeCopy HashNodeCopy;//节点拷贝函数指针
	Fun_HashTimeOut  HashTimeOut; //处理超时节点数据的函数指针
}WaHashMap;

#pragma pack(pop)


//-----------------------------------------------------
//说明: 初始化hashmap管理器,分配内存资源等
//输入: uMaxNodeCount 最大分配hash节点个数(注意计算空间,内部需要分配内存空间)
//输入: uDataNodeSize 数据节点空间大小
//输入: HashNodeCmp  节点比较函数指针
//输入: HashNodeKey  计算hash函数指针
//输入: HashNodeCopy 节点拷贝函数指针
//输入: HashTimeOut 处理超时节点数据的函数指针
//输入: pOutMsg 输出信息(失败时标识错误原因)
//输入: uMaxOutSize 最大输出空间大小
//返回: hashmap管理器指针
//-----------------------------------------------------
WaHashMap *InitHashMap(uint32 uMaxNodeCount, uint32 uDataNodeSize,
					   Fun_HashNodeCmp HashNodeCmp, Fun_HashNodeKey HashNodeKey,
					   Fun_HashNodeCopy HashNodeCopy, Fun_HashTimeOut HashTimeOut,
					   uint16 *pOutMsg, uint32 uMaxOutSize);

//-----------------------------------------------------
//说明: 释放hashmap管理器资源（释放内部内存资源）
//输入: hashmap管理器指针
//返回: 无
//-----------------------------------------------------
void FreeHashMap(WaHashMap **pHashMap);


//-----------------------------------------------------
//说明: 新建一个结点
//输入: pHashMap 管理器指针
//输入: pNode 需要插入的节点数据
//输入: u64LastTime 插入节点的时间(外部传入比较好,跑离线数据时用系统时间是错误的)
//返回: 插入的节点的指针,失败返回NULL
//-----------------------------------------------------
WaHashNode* HashBuildNode(WaHashMap *pHashMap, void *pDataNode, uint64 u64LastTime);


//-----------------------------------------------------
//说明: 插入一个节点
//输入: pHashMap 管理器指针
//输入: pNode 需要插入的节点
//返回: 插入的节点的指针,失败返回NULL
//-----------------------------------------------------
WaHashNode* HashInsetNode(WaHashMap *pHashMap, WaHashNode *pNode);


//-----------------------------------------------------
//说明: 将结点移除：从链表中移除,不是hash表中
//输入: pHashMap 管理器指针
//输入: pNode 需要移除的节点
//返回: 无
//-----------------------------------------------------
int32 HashRemoveNode(WaHashMap *pHashMap, WaHashNode *pNode);


//-----------------------------------------------------
//说明: 查找一个hash节点
//输入: pHashMap 管理器指针
//输入: pNode 需要查找的节点
//返回: 成功返回查找到的节点指针,失败返回NULL
//-----------------------------------------------------
WaHashNode *HashFindNode(WaHashMap *pHashMap, void *pDataNode);

//-----------------------------------------------------
//说明: 删除超时结点，调用指定的回调函数处理超时出来的数据
//输入: pHashMap 管理器指针
//输入: u64OutTime 设定多少秒算超时,设置为0就超时出所有
//输入: u64CurTime 当前时间
//返回: 返回超时的节点个数,失败返回-1
//-----------------------------------------------------
int32 HashDeleteTimeOut(WaHashMap *pHashMap, uint64 u64OutTime, uint64 u64CurTime);


//-----------------------------------------------------
//说明: 输出计数统计和查找冲突率计数
//输入: pHashMap 管理器指针
//输出: pStatMsg 输出格式化后的状态信息地址
//输入: uMaxOutSize 最大输出空间大小
//返回: 无
//-----------------------------------------------------
void GetHashRunStat(WaHashMap *pHashMap, uint16 *pStatMsg, uint32 uMaxOutSize);



#endif