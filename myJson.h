#ifndef  _MYJSON_H_
#define  _MYJSON_H
#pragma once

//#include "gbk2utf8.h"
#include "yyjson.h"
#include <Windows.h>
#define _CRT_SECURE_NO_WARNINGS

// 初始化的读写宏: 可以或
#define _PAUSE_JSON_      0x01
#define _WRITE_JSON_      0x10
#define _PAUSE_WRITE_JSON 0x11

class MyJson
{
public:

	MyJson();
	~MyJson();

	//-------------------------------------------------------------------------
	// 函数说明: 初始化
	// 输入: flag     请使用宏 _PAUSE_JSON_(解析) _WRITE_JSON_(生成)
	// 输入: json_str 如果flag == _PAUSE_JSON_，则为需要解析的JSON串,否则为NULL
	// 输出: 无
	// 返回：成功返回true 失败false
	//-------------------------------------------------------------------------
	bool Init(uint8_t flag, char *json_str = NULL);

	//-------------------------------------------------------------------------
	// 函数说明: 释放资源
	// 输入: 无
	// 输出: 无
	// 返回：无
	//-------------------------------------------------------------------------
	void Free(void);

	//-------------------------------------------------------------------------
	// 函数说明: 为JOSN串添加一对 key-value
	// 输入: key   键值
	// 输入: value 值(字符串)
	// 输出: 无
	// 返回：成功返回true 失败false
	//-------------------------------------------------------------------------
	bool AddRootKeyStr(const char *key, const char* value);

	//-------------------------------------------------------------------------
	// 函数说明: 为JOSN串添加一对 key-value
	// 输入: key 键值
	// 输入: value 值(数值)
	// 输出: 无
	// 返回：成功返回true 失败false
	//-------------------------------------------------------------------------
	bool AddRootKeyNum(const char *key, int value);

	//-------------------------------------------------------------------------
	// 函数说明:   为JOSN串添加JSON对象 {xxxxx}
	// 输入: obj   AddRootNullobj的返回值
	// 输出: 无
	// 返回：成功返回true 失败false
	//-------------------------------------------------------------------------
	bool AddRootobj(void* obj, const char *key);
	
	//-------------------------------------------------------------------------
	// 函数说明:   为JOSN串添加数组
	// 输入: arr   CreatJsonArrNum的返回值
	// 输出: 无
	// 返回：成功返回true 失败false
	//-------------------------------------------------------------------------
	bool AddRootArr(void *arr, const char *key);

	//-------------------------------------------------------------------------
	// 函数说明:   
	// 输入: key   键值
	// 输入: buf   值(yyjson_mut_val) 
	// 输入: elemCount   数组元素个数 
	// 输出: 无
	// 返回：成功返回true 失败NULL
	//-------------------------------------------------------------------------
	void* CreatJsonArrNum(const int32_t *buf, int elemCount);
	

	//-------------------------------------------------------------------------
	// 函数说明:  创建一个JSON对象{}
	// 输入: 无 
	// 输出: 无
	// 返回：成功返回true 失败NULL
	//-------------------------------------------------------------------------
	void* CreatJsonObj();

	//-------------------------------------------------------------------------
	// 函数说明: 往数组中添加val
	// 输入: arr CreatJsonArrNum的返回值
	// 输入: val yyjson_mut_val类型
	// 输出: 无
	// 返回：成功返回true 失败NULL
	//-------------------------------------------------------------------------
	bool AddArrVal(void *arr , void *val);

	//-------------------------------------------------------------------------
	// 函数说明: 往JSON对象中添加key-vale
	// 输入: val CreatJsonObj的返回值
	// 输入: key   键值
	// 输入: value 字符串值
	// 输出: 无
	// 返回：成功返回true 失败NULL
	//-------------------------------------------------------------------------
	bool AddObjKeyStr(void* val, const char* key, const char* value);

	//-------------------------------------------------------------------------
	// 函数说明: 往JSON对象中添加key-vale
	// 输入: val CreatJsonObj的返回值
	// 输入: key   键值
	// 输入: value 数字值
	// 输出: 无
	// 返回：成功返回true 失败NULL
	//-------------------------------------------------------------------------
	bool AddObjKeyNum(void* val, const char* key, int value);

	// 转为JOSN字符串
	//-------------------------------------------------------------------------
	// 函数说明: 往JSON转成JSON字符串
	// 输入: 无
	// 输出: json_str 生成的结果
	// 返回：成功返回true 失败false
	//-------------------------------------------------------------------------
	bool printJsonStr(char *json_str);

public:

	// 解析JSON
	
	// 从JSON中获取str类型的值
	bool PauseRootKeyStr(const char *key, char *val, int vallen);
	
	// 从JSON中获取int类型的值
	int PauseRootKeyNum(const char *key);

	// 从JSON中获取Arr类型的值
	void* PauseRootKeyArr(const char *key);

	// 从JSON中获取Obj类型的值
	void* PauseRootKeyObj(const char *key);

	// 从JSON数组中获取元素 -- str
	bool PauseArrElemStr(void *arr, char* buf[], int buflen, int bufCount);

	// 从JSON数组中获取元素 -- arr
	void* PauseArrElemArr(void *arr) {}

	// 从JSON数组中获取元素 -- int
	bool PauseArrElemNum(void *arr, int *buf, int bufCount);

	// 从JSON数组中获取JSON对象 -- str
	bool PauseArrElemObj(void *arr, void *buf[], int bufCount);

	// 从JSON对象中取值 -- str
	bool PauseObjElemStr(void *obj, const char *key, char *val, int vallen);

	// 从JSON对象中取值 -- int
	int PauseObjElemNum(void *obj, const char *key);

private:
	
	// yyJson支持的编码格式为UTF-8,windows下默认为GBK编码/Linux下默认为UTF-8编码
	bool IsStrUtf8(const char* str);
	bool IsStrGbk(const char* str);
	int  GBK2UTF8(char *szGbk, char *szUtf8, int Len);
	int  UTF82GBK(char *szUtf8, char *szGbk, int Len);

private:
    
	//----------------------解析JSON--------------------//
	yyjson_doc* m_doc;
	yyjson_val* m_root;

	//----------------------生成JSON--------------------//
	yyjson_mut_doc* m_mut_doc;
	yyjson_mut_val* m_mut_root;
	
};


#endif

